package com.dev.stream.socket;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.logging.Level;
import java.util.logging.Logger;


public class StreamHandler implements WebSocketHandler {

	private Logger logger = Logger.getLogger(StreamHandler.class.getName());

	@Override
	public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
		logger.log(Level.INFO,"Connection established");
	}

	@Override
	public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage)
			throws Exception {
		System.out.println("handle");

	}

	@Override
	public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {

	}

	@Override
	public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
		webSocketSession.close();
		logger.log(Level.SEVERE,"Connection is closed");
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}
}
